describe('my first test', function () {
  it(
    'this is a passing test', function () {
      expect(true).to.equal(true)
    }
  );
  it(
    'this is not a passing test', function () {
      expect(true).to.equal(false)
    }
  );
});

describe('Home', function () {
  it('should load page', function() {
    cy.visit('/')
  });
  it('should allow for internal navigation', function() {
    cy.contains('Home').click()
  });
  it('should allow for internal navigation', function() {
    cy.contains('Tour').click()
  });
  it('should allow for internal navigation', function() {
    cy.contains('Merch').click()
  });
  it('should allow for internal navigation', function() {
    cy.contains('Albums').click()
  });
  it('should allow for internal navigation', function() {
    cy.contains('Band').click()
  });
  it('should allow for internal navigation', function() {
    cy.contains('Media').click()
  });
  it('should allow for internal navigation', function() {
    cy.contains('News').click()
  });
});
