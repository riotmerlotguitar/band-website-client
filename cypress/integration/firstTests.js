// Simple boolean checking
describe('Testing', () => {
  it('should pass', function () {
    expect(true).to.equal(true)

  });
});


// Testing of 'Buy' button press
describe('Homepage', function () {
  it('should load page', function () {
    cy.visit('/tour');
    cy.contains('Buy').click();
    // cy.get('#closeBtn').click();
    // cy.url().should('include', '/tour'); Use this for navigating links
  });
});


// Testing of Tour Date Label
describe('Tour Date Label', function () {
  it('should see initial tour date', function () {
    cy.visit('/tour');
    cy.contains('2019-05-01').click();
  });
});


// Testing of Tour Date Label part2
describe('Tour Date Label', function () {
  it('should see second tour date', function () {
    cy.visit('/tour');
    cy.contains('2019-05-12').click();
  });
});


// Testing of 'Click me' button press
describe('News Click Button', function () {
  it('You should be able to click the \'click me\' button', function () {
    cy.visit('/news');
    cy.contains('Click me!').click();
    // cy.get('#closeBtn').click();
    // cy.url().should('include', '/tour'); Use this for navigating links
  });
});


// Testing of 'Click me' button press
describe('News Click Button Again', function () {
  it('You should be able to click the \'click me again\' button', function () {
    cy.visit('/news');
    cy.contains('Click me again!').click();
    // cy.get('#closeBtn').click();
    // cy.url().should('include', '/tour'); Use this for navigating links
  });
});


// // Change to personal band project
// describe('Create a student', function () {
//   it('should allow for creation of a student', function () {
//     cy.visit('/create');
//     cy.get('#name').type('Bob');
//     cy.get('#age').type('12');
//     cy.get('#gender').type('Male');
//     cy.get('#year').select(1);
//     cy.contains('Add Student').click();
//
//   })
// });
