import { Component, OnInit } from '@angular/core';
import { ComService } from '../com.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  student: any[];

  constructor(private Service: ComService) { }

  private tourDates: Array<any>;
  private newsItems: Array<any>;

  ngOnInit() {
    this.Service.getAllTourDates().subscribe(data => this.tourDates = data);
    this.Service.getAllNews().subscribe(data => this.newsItems = data);
  }

}
