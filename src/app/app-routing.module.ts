import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TourComponent } from './tour/tour.component';
import { MerchandiseComponent } from './merchandise/merchandise.component';
import { AlbumsComponent } from './albums/albums.component';
import { BandComponent } from './band/band.component';
import { MediaComponent } from './media/media.component';
import { NewsComponent } from './news/news.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'tour', component: TourComponent},
  {path: 'merchandise', component: MerchandiseComponent},
  {path: 'albums', component: AlbumsComponent},
  {path: 'band', component: BandComponent},
  {path: 'media', component: MediaComponent},
  {path: 'news', component: NewsComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
