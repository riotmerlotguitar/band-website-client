import { Component, OnInit } from '@angular/core';
import {ComService} from '../com.service';

@Component({
  selector: 'app-band',
  templateUrl: './band.component.html',
  styleUrls: ['./band.component.css']
})
export class BandComponent implements OnInit {

  // ***** Member Variables *****
  bandDatabase: Array<any>;

  // ***** Constructors *****
  constructor(public comService: ComService) {
  }

  ngOnInit() {
    this.comService.getAllBandInfo().subscribe(band => { this.bandDatabase = band; },
      () => {}, () => {});
  }

  // ***** Functions *****
  printHello(id): void {
    console.log('Hello world!' + id);
  }

}

