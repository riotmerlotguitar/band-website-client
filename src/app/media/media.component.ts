import { Component, OnInit } from '@angular/core';
import {ComService} from '../com.service';

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})
export class MediaComponent implements OnInit {

  // ***** Member Variables *****
  mediaDatabase: Array<any>;

  // ***** Constructors *****
  constructor(public comService: ComService) {
  }

  ngOnInit() {
    this.comService.getAllMedia().subscribe(band => { this.mediaDatabase = band; },
      () => {}, () => {});
  }

  // ***** Functions *****
  printHello(id): void {
    console.log('Hello world!' + id);
  }

}
