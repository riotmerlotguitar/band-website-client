import { Component, OnInit } from '@angular/core';
import {ComService} from '../com.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {
  albums: any[];
  tracks: any[];

  constructor(private Service: ComService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getAlbumInfo();
    // const id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    // this.getAlbumTracks(id);
  }
  getAlbumInfo() {
    this.Service.getAllAlbums().subscribe( data => {
      this.albums = data;
    });
  }
  getAlbumTracks(id: number) {
    this.Service.getAlbumTracks(id).subscribe( data => {
      this.tracks = data as any[];
      console.log(this.tracks);
    });
  }
}
