import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MatDividerModule, MatTableModule } from "@angular/material";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComService } from './com.service';
import { HomeComponent } from './home/home.component';
import { AlbumsComponent } from './albums/albums.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TourComponent } from './tour/tour.component';
import { MerchandiseComponent } from './merchandise/merchandise.component';
import { BandComponent } from './band/band.component';
import { MediaComponent } from './media/media.component';
import { NewsComponent } from './news/news.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AlbumsComponent,
    HomeComponent,
    NavbarComponent,
    TourComponent,
    MerchandiseComponent,
    BandComponent,
    MediaComponent,
    NewsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MatTableModule,
    MatDividerModule
  ],
  providers: [ComService],
  bootstrap: [AppComponent]
})
export class AppModule { }
