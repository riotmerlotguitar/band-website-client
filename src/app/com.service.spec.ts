import { TestBed } from '@angular/core/testing';

import { ComService } from './com.service';
import {HttpClientModule} from "@angular/common/http";

describe('ComService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ComService],
    imports: [HttpClientModule],
  }));

  it('should be created', () => {
    const service: ComService = TestBed.get(ComService);
    expect(service).toBeTruthy();
  });
});
