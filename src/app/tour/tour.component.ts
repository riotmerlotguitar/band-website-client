import { Component, OnInit } from '@angular/core';
import { MatTab } from "@angular/material";
import {ComService} from "../com.service";

@Component({
  selector: 'app-tour',
  templateUrl: './tour.component.html',
  styleUrls: ['./tour.component.css']
})
export class TourComponent implements OnInit {

  constructor(private Service: ComService) { }

  private tourDates: Array<any>;
  private displayedTourColumns: string[] = ['date', 'country', 'state', 'city', 'venue', 'buyBtn'];
  private showBuyPopup: boolean;
  private tourToBuy;

  ngOnInit() {
    this.showBuyPopup = false;
    this.Service.getAllTourDates().subscribe(data => this.tourDates = data);
  }

  private buyTickets(tour: any) {
    this.tourToBuy = tour;
    this.showBuyPopup = true;
  }

  private closeBuyTickets() {
    this.showBuyPopup = false;
  }

}
