import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourComponent } from './tour.component';
import {MatTableModule} from "@angular/material";
import {HttpClientModule} from "@angular/common/http";

describe('TourComponent', () => {
  let component: TourComponent;
  let fixture: ComponentFixture<TourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourComponent ], imports: [MatTableModule, HttpClientModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
