import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComService {
  private rootUrl = '//localhost:8080';

  constructor(private http: HttpClient) { }

  public getAllNews(): Observable<any> {
    return this.http.get(this.rootUrl + '/news');
  }

  public getAllTourDates(): Observable<any> {
    return this.http.get(this.rootUrl + '/tour_dates');
  }

  public getAllMerch(): Observable<any> {
    return this.http.get(this.rootUrl + '/merch');
  }

  public getAllBandInfo(): Observable<any> {
    return this.http.get(this.rootUrl + '/band_info');
  }

  public getAllAlbums(): Observable<any> {
    return this.http.get(this.rootUrl + '/albums');
  }

  public getAllTrackInfo(): Observable<any> {
    return this.http.get(this.rootUrl + '/tracks');
  }

  public getAllMedia(): Observable<any> {
    return this.http.get(this.rootUrl + '/media');
  }

  public getAlbumTracks(id: any): Observable<any> {
    return this.http.get(this.rootUrl + '/tracks/' + id);
  }

}
